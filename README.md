# DEPRECATED
gitlab.gwdg.de and gitlab-ce.gwdg.de can pull gcr.io/kaniko-project/executor:debug directly now.
So please update your repositories to pull `gcr.io/kaniko-project/executor:debug` instead of `$CI_REGISTRY/j.hoerdt/kaniko`.
I am not sure for how much longer long this repository can exist, since my student account may get deleted.

```diff
 buildimage:
   image:
-    name: $CI_REGISTRY/j.hoerdt/kaniko
+    name: gcr.io/kaniko-project/executor:debug
     entrypoint: [""]
```

# kaniko
This repository serves the purpose of mirroring the [kaniko](https://github.com/GoogleContainerTools/kaniko) debug image into the GWDG's GitLab container registry. Kaniko is a tool for building docker images without access to the docker daemon. The GWDG's GitLab prevents access to the docker daemon in CI pipelines[[1](https://info.gwdg.de/faq/index.php?action=artikel&cat=57&id=274&artlang=de)]. Kaniko can be used to build images instead, as described [here](https://docs.gitlab.com/ce/ci/docker/using_kaniko.html) or as demonstrated in this repository. Unfortunately the GWDG's GitLab CI runners won't pull from gcr.io where the kaniko images are published. The runners can pull the kaniko image from this repository's container registry instead.

## Usage
Please have a look at [.gilab-ci.yml](.gilab-ci.yml) for an example on how to build images using kaniko. This repository uses kaniko itself to mirror the kaniko image. You may not even need to change anything. By default this CI configuration creates three tags for the built image:
- `latest`
- `$CI_COMMIT_SHORT_SHA` eg. c233d07d
- `$CI_COMMIT_REF_NAME` eg. master

I recommend in production you only pull images tagged with your master or main branch instead of the `latest` tag. If you follow this practice you can simply drop the
```yml
  only:
    refs:
      - master

```
restriction for the CI to build images for all branches and merge requests. If you need to use the `latest` tag make sure only your production branch creates it.

Make sure to turn on image cleanup in your project settings, otherwise kaniko's cache and the commit sha tags will grow indefinitely .

![screengrab of gitlab image cleanup settings](gitlab_clean_settings.svg)